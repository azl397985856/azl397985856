<img src="https://github-readme-stats.vercel.app/api?username=azl397985856&show_icons=true" alt="logo" height="160" align="right" style="margin: 5px; margin-bottom: 20px;" />

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=azl397985856)](https://github.com/azl397985856/leetcode)

In 2021, I made 980 contributions and modified 4,000 lines of code. Check out my GitHub Wrapped and create your own at githubtrends.io/wrapped/azl397985856

## ヾ(@^▽^@)ノ

- 🔭 I’m currently working on  [91 algorthimn](https://lucifer.ren/blog/2021/12/03/91algo-6/)
- 🌱 I’m currently learning psychology
- 👯 I’m looking to collaborate on 91 team. feel free to reach me: azl397985856@gmail.com
- 💬 Ask me about frontend, algorthimn, interview etc

##  Follow Me

- [微信公众号 【力扣加加】](https://tva1.sinaimg.cn/large/007S8ZIlly1gfcuzagjalj30p00dwabs.jpg) 算法方向
- [微信公众号 【脑洞前端】](https://tva1.sinaimg.cn/large/007S8ZIlly1gfxro1x125j30oz0dw43s.jpg) 前端方向
- [知乎](https://www.zhihu.com/people/lu-xiao-13-70)
- [Blog](https://lucifer.ren/blog/)
- [力扣加加网站](http://leetcode-solution.cn/) 

<img src="https://github-profile-trophy.vercel.app/?username=azl397985856&theme=flat&column=7" alt="logo" height="160" align="center" style="margin: auto; margin-bottom: 20px;" />

## 📕 Latest Blog Posts

<!-- BLOG-POST-LIST:START -->
- [远程办公有多爽？](https://lucifer.ren/blog/2022/02/20/wfh2/)
- [跨域了？ 装个插件就够了！](https://lucifer.ren/blog/2022/02/06/cors-extension/)
- [听说逆向思维能够降低时间复杂度？](https://lucifer.ren/blog/2022/01/27/backward/)
- [技术面试原来不止考技术？](https://lucifer.ren/blog/2022/01/04/non-tech-skill/)
- [刷题插件可以隐藏测试用例啦](https://lucifer.ren/blog/2021/12/22/leetcode-cheat-hide-cases/)
<!-- BLOG-POST-LIST:END -->


